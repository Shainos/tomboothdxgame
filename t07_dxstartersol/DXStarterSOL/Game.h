#ifndef GAME_H
#define GAME_H

#include <string>
#include <ctime>
#include <vector>
#include "Input.h"
#include "GameObj.h"

class Game
{
public:
	Game();
	~Game();
	void Initialise();
	void Update(float timedif);
	void Progress();
	std::string GetMsg(int msgval);
	static MouseAndKeys sMKIn;
	static Gamepads sGamepads;
	enum GameState
	{
		PREGAME,
		PLAYING,
		ENDGAME
	};
	std::vector<GameObj> Objects[10];
	GameState Current;

private:
	int score = 0;
	float timer = 0.f;
	//messages to be dispalyed to the user
	std::string message1;
	std::string message2;
	std::string message3;
};
#endif // !GAME_H