#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <iomanip>
#include <ctime>

#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "SimpleMath.h"
#include "SpriteFont.h"
#include "Game.h"
#include "Input.h"
#include "TexCache.h"
#include "Sprite.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;


DirectX::SpriteFont *gpFont = nullptr;
DirectX::SpriteBatch *gpSpriteBatch = nullptr;

float gResTimer = 0;
int gFrameCounter = 0;
float gFrameTimer = 0;

char lastChar;
float inputDelay;

Game game;


void InitGame(MyD3D& d3d)
{
	gpSpriteBatch = new SpriteBatch(&d3d.GetDeviceCtx());
	assert(gpSpriteBatch);

	gpFont = new SpriteFont(&d3d.GetDevice(), L"../bin/data/algerian.spritefont");
	assert(gpFont);

	game.Initialise();

	TexCache& cache = d3d.GetCache();
	cache.LoadTexture(&d3d.GetDevice(), "SpaceBackground.dds");
	cache.LoadTexture(&d3d.GetDevice(), "ship.dds");
	cache.LoadTexture(&d3d.GetDevice(), "missile.dds");

}


//any memory or resources we made need releasing at the end
void ReleaseGame()
{
	delete gpFont;
	gpFont = nullptr;

	delete gpSpriteBatch;
	gpSpriteBatch = nullptr;
}

//called over and over, use it to update game logic
void Update(float dTime, MyD3D& d3d)
{

	if (inputDelay > 0)
		inputDelay =  inputDelay - dTime;
	
	game.Update(dTime);

}

//called over and over, use it to render things
void Render(float dTime, MyD3D& d3d)
{
	WinUtil& wu = WinUtil::Get();
	
	Vector4 colour = { 0.0f, 0.0f, 0.0f, 1.0f };//black
	TexCache& cache = d3d.GetCache();
	d3d.BeginRender(colour);
	gpSpriteBatch->Begin();


	for (int i = 0; i < 10; i++)
	{
		Sprite spr(d3d);
		spr.SetTex(*d3d.GetCache().Get(game.Objects->at(i).GetTexName()).pTex);
		spr.SetTexRect(RECTF{ game.Objects->at(i).GetTexArea().left, game.Objects->at(i).GetTexArea().top, game.Objects->at(i).GetTexArea().right, game.Objects->at(i).GetTexArea().bottom });
		spr.SetOrigin(Vector2(game.Objects->at(i).GetOrigin().x, game.Objects->at(i).GetOrigin().y));
		spr.SetScale(Vector2(game.Objects->at(i).GetScale().x, game.Objects->at(i).GetScale().y));
		spr.SetPos(Vector2(game.Objects->at(i).GetPos().x, game.Objects->at(i).GetPos().y ));
		spr.SetRot(game.Objects->at(i).GetRot());

		spr.Draw(*gpSpriteBatch);
	}

	gpSpriteBatch->End();
	d3d.EndRender();
}

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	gResTimer = GetClock() + 2;
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27: //escape key
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		
			
		}
		if (inputDelay < 0)
			lastChar = NULL;
	case WM_INPUT:
		game.sMKIn.MessageEvent((HRAWINPUT)lParam);
		break;
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(1024), h(768);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "DX_SIXES", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	d3d.GetCache().SetAssetPath("../bin/data/");
	InitGame(d3d);

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			Update(dTime, d3d);
			Render(dTime, d3d);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
		gFrameCounter++;
	}

	ReleaseGame();
	d3d.ReleaseD3D(true);	
	return 0;
}