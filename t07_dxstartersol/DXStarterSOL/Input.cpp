#include <math.h>
#include <cassert>
#include <sstream>


#include "Input.h"
#include "D3D.h"
#include "D3DUtil.h"
#include "WindowUtils.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;



void MouseAndKeys::Initialise(HWND hwnd, bool showMouse, bool confineMouse)
{
	mHwnd = hwnd;

	RAWINPUTDEVICE Rid[2];

	//these ids trigger mouse input
	Rid[0].usUsagePage = 0x01;
	Rid[0].usUsage = 0x02;
	Rid[0].dwFlags = RIDEV_INPUTSINK;// RIDEV_NOLEGACY;   // adds HID mouse and also ignores legacy mouse messages
	Rid[0].hwndTarget = hwnd;

	//these ids trigger keyboard input
	Rid[1].usUsagePage = 0x01;
	Rid[1].usUsage = 0x06;
	Rid[1].dwFlags = RIDEV_INPUTSINK;// RIDEV_NOLEGACY;   // adds HID keyboard and also ignores legacy keyboard messages
	Rid[1].hwndTarget = hwnd;

	// RIDEV_NOLEGACY will stop normal message pump WM_CHAR type messages
	// sometimes we might want that, for now we'll leave it, then we can
	// still use the normal message pump for things like player name entry

	if (RegisterRawInputDevices(Rid, 2, sizeof(Rid[0])) == FALSE) {
		MessageBox(0, "Cannot get keyboard and mouse input", 0, 0);
		assert(false);
	}

	ShowCursor(showMouse);
	mConfineMouse = confineMouse;
	GetClipCursor(&mOldClip);
	if (mConfineMouse)
	{
		GetWindowRect(hwnd, &mNewClip);
		ClipCursor(&mNewClip);
	}
	GetMousePosAbsolute(mMouseScreen);
}
//-------------------------------------------------------------
bool MouseAndKeys::IsPressed(unsigned short vkeyCode) const
{
	assert(vkeyCode < KEYBUFF_SIZE);
	bool pressed = mKeyBuffer[vkeyCode] != 0;

	return pressed;
}

void MouseAndKeys::ProcessKeys(RAWINPUT* raw)
{
	//standard key code
	unsigned short flags = raw->data.keyboard.Flags;
	unsigned short vkey = raw->data.keyboard.VKey;
	//the scan code might be useful, but we won't use it here
	unsigned short scanCode = raw->data.keyboard.MakeCode;

	//ignore anything larger than this, escape codes, fake keys or weird keys
	if (vkey >= 255)
		return;

	// e0 and e1 are escape sequences used for certain special keys, such as PRINT and PAUSE/BREAK.
	// see http://www.win.tue.nl/~aeb/linux/kbd/scancodes-1.html
	const bool isE0 = ((flags & RI_KEY_E0) != 0);
	const bool isE1 = ((flags & RI_KEY_E1) != 0);

	if (isE1)
	{
		// for escaped sequences, turn the virtual key into the correct scan code using MapVirtualKey.
		// however, MapVirtualKey is unable to map VK_PAUSE (this is a known bug), hence we map that by hand.
		if (vkey == VK_PAUSE)
			scanCode = 0x45;
		else
			scanCode = MapVirtualKey(vkey, MAPVK_VK_TO_VSC);
	}


	switch (vkey)
	{
		// NUMPAD ENTER has its e0 bit set
	case VK_RETURN:
		if (isE0)
			vkey = VK_NUMPAD_ENTER; //numpad enter
		break;
	}
	//ignore anything larger than this, escape codes, fake keys or weird keys
	if (vkey >= 255)
		return;

	if (flags & RI_KEY_BREAK) //key up
		mKeyBuffer[vkey] = 0;
	else
		mKeyBuffer[vkey] = scanCode;
}

void MouseAndKeys::ProcessMouse(RAWINPUT* raw)
{
	unsigned short flags = raw->data.mouse.usButtonFlags;

	if (flags & RI_MOUSE_LEFT_BUTTON_DOWN)
		mButtons[LBUTTON] = true;
	if (flags & RI_MOUSE_LEFT_BUTTON_UP)
		mButtons[LBUTTON] = false;


	Vector2 last(mMouseScreen);
	GetMousePosAbsolute(mMouseScreen);
	mMouseMove = mMouseScreen - last;
}

void MouseAndKeys::GetMousePosAbsolute(Vector2& pos)
{
	POINT mpos;
	if (GetCursorPos(&mpos))
	{
		if (ScreenToClient(mHwnd, &mpos))
		{
			pos = Vector2((float)mpos.x, (float)mpos.y);
		}
	}

}
void MouseAndKeys::MessageEvent(HRAWINPUT rawInput)
{
	UINT dwSize;

	UINT res = GetRawInputData(rawInput, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER));
	if (res != 0)
		return;//bad input so ignore it

	if (dwSize >= RAWBUFF_SIZE)
	{
		DBOUT("Buffer too small. Is " << RAWBUFF_SIZE << " wants " << dwSize);
		assert(false);
	}

	if (GetRawInputData(rawInput, RID_INPUT, mInBuffer, &dwSize, sizeof(RAWINPUTHEADER)) != dwSize)
	{
		DBOUT("GetRawInputData failed");
		assert(false);
	}

	RAWINPUT* raw = (RAWINPUT*)mInBuffer;

	if (raw->header.dwType == RIM_TYPEKEYBOARD)
	{
		ProcessKeys(raw);
	}
	else if (raw->header.dwType == RIM_TYPEMOUSE)
	{
		ProcessMouse(raw);
	}
}

void Gamepads::Update()
{
	for (DWORD i = 0; i < XUSER_MAX_COUNT; i++)
	{
		State& s = mPads[i];
		s.port = -1;
		ZeroMemory(&s.state, sizeof(XINPUT_STATE));
		if (XInputGetState(i, &s.state) == ERROR_SUCCESS)
		{
			float normLX = fmaxf(-1, (float)s.state.Gamepad.sThumbLX / 32767);
			float normLY = fmaxf(-1, (float)s.state.Gamepad.sThumbLY / 32767);

			s.leftStickX = (abs(normLX) < s.deadzoneX ? 0 : (abs(normLX) - s.deadzoneX) * (normLX / abs(normLX)));
			s.leftStickY = (abs(normLY) < s.deadzoneY ? 0 : (abs(normLY) - s.deadzoneY) * (normLY / abs(normLY)));

			if (s.deadzoneX > 0)
				s.leftStickX *= 1 / (1 - s.deadzoneX);
			if (s.deadzoneY > 0)
				s.leftStickY *= 1 / (1 - s.deadzoneY);

			s.port = i;
		}
	}

}

void Gamepads::Initialise()
{
	for (int i = 0; i < XUSER_MAX_COUNT; ++i)
		mPads[i].port = -1;
}


bool Gamepads::IsPressed(int idx, unsigned short buttonId)
{
	assert(idx >= 0 && idx < XUSER_MAX_COUNT);
	assert(mPads[idx].port != -1);
	return (mPads[idx].state.Gamepad.wButtons & buttonId) != 0;
}