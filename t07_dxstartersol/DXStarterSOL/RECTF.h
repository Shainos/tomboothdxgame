#pragma once

#include "SpriteBatch.h"
/*
Used to specify areas within sprites for rendering
*/
struct RECTF
{
	float left, top, right, bottom;
	operator RECT() {
		return RECT{ (int)left,(int)top,(int)right,(int)bottom };
	}
};