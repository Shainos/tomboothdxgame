#include "Game.h"

#include "WindowUtils.h"
MouseAndKeys Game::sMKIn;
Gamepads Game::sGamepads;

Game::Game()
{
	for (int i = 0; i < 10; i++)
	{
		Objects->push_back(GameObj());
		if (i == 0)
		{
			Objects->at(i).Initialise(MYVEC2{ WinUtil::Get().GetClientWidth() / 2.f,  WinUtil::Get().GetClientHeight() / 2.f }, 0.f, "SpaceBackground", MYVEC2{ 426.f, 240.f }, MYRECT{ 0.f, 0.f, 852.f, 480.f }, MYVEC2{ 1.f,1.f });
		}
		else if (i == 1)
		{
			Objects->at(i).Initialise(MYVEC2{ WinUtil::Get().GetClientWidth() / 2.f, WinUtil::Get().GetClientHeight() / 2.f }, 0.f, "ship", MYVEC2{ 256.f,256.f }, MYRECT{ 0.f, 0.f,512.f,512.f}, MYVEC2{ 1.f,1.f });
		}
		else
		{
			Objects->at(i).Initialise(MYVEC2{ WinUtil::Get().GetClientWidth() / 2.f, WinUtil::Get().GetClientHeight() / 2.f }, 0.f, "missile", MYVEC2{ 27.f, 24.f }, MYRECT{ 0.f, 0.f, 55.f, 47.f }, MYVEC2{ 1.f,1.f });

		}
			
	}

}

Game::~Game()
{

}
//initialise game values
void Game::Initialise()
{
	srand(static_cast<unsigned>(time(0)));


	Current = GameState::PLAYING;
}

//deteermines the message to be displayed depending on what state the game is in
void Game::Update(float timedif)
{
	sGamepads.Update();
	switch (Current)
	{
	case Game::PREGAME:
	case Game::PLAYING:
		timer += timedif;
		if (timer >= 100.f)
		{
			score += 1;
			timer -= 100.f;
		}
		if (sMKIn.IsPressed(VK_UP))
		{
			Objects->at(1).SetPos(MYVEC2{ Objects->at(1).GetPos().x,Objects->at(1).GetPos().y + 1 });
		}
		if (sMKIn.IsPressed(VK_DOWN))
		{
			Objects->at(1).SetPos(MYVEC2{ Objects->at(1).GetPos().x,Objects->at(1).GetPos().y - 1 });
		}
		if (sMKIn.IsPressed(VK_LEFT))
		{
			Objects->at(1).SetPos(MYVEC2{ Objects->at(1).GetPos().x - 1,Objects->at(1).GetPos().y });
		}
		if (sMKIn.IsPressed(VK_RIGHT))
		{
			Objects->at(1).SetPos(MYVEC2{ Objects->at(1).GetPos().x + 1,Objects->at(1).GetPos().y });
		}
	case Game::ENDGAME:

	default:
		break;
	}

	
}
//retrieves the message so it can be displayed
std::string Game::GetMsg(int msgval)
{
	if (msgval == 1)
	{
		return message1;
	}
	else if (msgval == 2)
	{
		return message2;
	}
	else
	{
		return message3;
	}
}

//checks moves the program onto the next part
void Game::Progress()
{


}
