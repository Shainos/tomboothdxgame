#ifndef INPUT_H


#define INPUT_H


#include <Windows.h>
#include <Xinput.h>
#include <string>

#include "D3D.h"
#include "SimpleMath.h"


#define VK_NUMPAD_ENTER 0xF0


class MouseAndKeys
{
public:
	MouseAndKeys()
	{
		Reset();

	}
	void Reset()
	{
		ZeroMemory(mInBuffer, sizeof(mInBuffer));
		ZeroMemory(mKeyBuffer, sizeof(mKeyBuffer));
		mButtons[0] = mButtons[1] = mButtons[2] = false;
		mMouseScreen = mMouseMove = DirectX::SimpleMath::Vector2(0, 0);
	}
	//start up the mouse/keys system, call once only
//hwnd - main windows handle
//showMouse - hide the mouse cursor
//confineMouse - prevent the mouse leaving the window
	void Initialise(HWND hwnd, bool showMouse = true, bool confineMouse = false);
	//is a specific key down?
	bool IsPressed(unsigned short vkeyCode) const;
	//check if a button is currently down
	typedef enum { LBUTTON = 0, MBUTTON = 1, RBUTTON = 2 } ButtonT;
	bool GetMouseButton(ButtonT b) const {
		return mButtons[b];
	}
	//get relative or absolute mouse position
//absolute=true, 2D windows coordinates of cursor
//absolute=false, 2D windows coordinate change in position since last update (push)
	DirectX::SimpleMath::Vector2 GetMousePos(bool absolute) const {
		if (absolute)
			return mMouseScreen;
		return mMouseMove;
	}
	//add to your windows message pump
//rawInput comes from lparam after a WM_INPUT message
//case WM_INPUT:
//	input.MessageEvent((HRAWINPUT)lParam);
	void MessageEvent(HRAWINPUT rawInput);
private:
	//copy of main window handle
	HWND mHwnd;
	enum { RAWBUFF_SIZE = 512, KEYBUFF_SIZE = 255, KMASK_IS_DOWN = 1, MAX_BUTTONS = 3 };
	//raw input buffer
	BYTE mInBuffer[RAWBUFF_SIZE];
	unsigned short mKeyBuffer[KEYBUFF_SIZE];  //virtual key codes, but only the standard ones (first 256)
	//support for three mouse buttons
	bool mButtons[MAX_BUTTONS];
	//track mouse position changes (2D)
	DirectX::SimpleMath::Vector2 mMouseScreen, mMouseMove;
	//if the mouse is confined, track the size of the window
	bool mConfineMouse;
	RECT mNewClip;           // new area for ClipCursor
	RECT mOldClip;        // previous area for ClipCursor

	void ProcessKeys(RAWINPUT* raw);
	void ProcessMouse(RAWINPUT* raw);
	void GetMousePosAbsolute(DirectX::SimpleMath::Vector2& pos);
};

class Gamepads
{
public:
	//gamepad data, one per pad
	struct State
	{
		int port = -1;
		float leftStickX = 0;
		float leftStickY = 0;
		float rightStickX = 0;
		float rightStickY = 0;
		float leftTrigger = 0;
		float rightTrigger = 0;
		float deadzoneX = 0.1f;
		float deadzoneY = 0.1f;
		XINPUT_STATE state;
	};
	//as a mechanical device the pad may generate data even when
//nobody is touching it, especially if it's old. The deadzone
//specifies a small range of input that will be ignored.
//idx=which pad
//x+y=range to ignore
	void SetDeadZone(int idx, float x, float y) {
		assert(idx >= 0 && idx < XUSER_MAX_COUNT);
		mPads[idx].deadzoneX = x;
		mPads[idx].deadzoneY = y;
	}
	//look at the state of a pad
	//idx=pad 0,1,2 or 3
	const State& GetState(int idx) const {
		assert(idx >= 0 && idx < XUSER_MAX_COUNT);
		return mPads[idx];
	}
	//called once at startup
	void Initialise();
	//check that a specific pad is still plugged in
	bool IsConnected(int idx) {
		assert(idx >= 0 && idx < XUSER_MAX_COUNT);
		return mPads[idx].port != -1;
	}
	//called every update
	void Update();

	/*
	Is the user holding down a button or stick
	XINPUT_GAMEPAD_DPAD_UP
	XINPUT_GAMEPAD_DPAD_DOWN
	XINPUT_GAMEPAD_DPAD_LEFT
	XINPUT_GAMEPAD_DPAD_RIGHT
	XINPUT_GAMEPAD_START
	XINPUT_GAMEPAD_BACK
	XINPUT_GAMEPAD_LEFT_THUMB
	XINPUT_GAMEPAD_RIGHT_THUMB
	XINPUT_GAMEPAD_LEFT_SHOULDER
	XINPUT_GAMEPAD_RIGHT_SHOULDER
	XINPUT_GAMEPAD_A
	XINPUT_GAMEPAD_B
	XINPUT_GAMEPAD_X
	XINPUT_GAMEPAD_Y
	*/
	bool IsPressed(int idx, unsigned short buttonId);
private:
	//a copy of state for each of 4 pads
	State mPads[XUSER_MAX_COUNT];
};

#endif // !INPUT_H

